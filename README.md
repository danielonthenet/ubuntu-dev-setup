# ubuntu-dev-setup

This reppository contains a set of Ansible playbooks to configure a ubuntu machine for dev purposes, after a clean installation. The playbooks should run in most Debian based system but was only tested with **Ubuntu 18.04**

## Pre-requisites

- The following packages need to be installed
  - Ansible
  - Git
- If they are not already installed, install them by running the following in a terminal window

```bash
sudo apt install ansible git
```

## Running the playbooks to configure your system

Run the playbook as the primary user (yourself), not as `root` or with `sudo`.

```bash
#If you are setting this up on WSL (Ubuntu), substitue `$(logname)` with your Ubuntu primary username
ansible-playbook site.yml -e "local_username=$(logname)" -b -K
```

## What gets installed and cofigured

Many tools that are useful for developers / IT engineers are installed. However, since every person would have their own preference for tools to use, feel free to fork the repo and update the code to include the tools you use.

### Here is a list of what are installed / configured

- Most tools installed as packages are listed here: `group_vars/all/base.yml`
- Additionally, following tools are also installed:
  - vim
  - kubectl
  - Azure CLI
  - vscode (optional)
  - Ansible Azure modules
  - zsh shell, antigen (plugin manager for zsh), oh-my-zsh plugins
  - Powerline Fonts
- `.zshrc` file is generated with following configurations:
  - `oh-my-zsh` and some bundled plugins
  - powerlevel9k zsh theme
  - Function to stop ssh-agent from asking for encrypted ssh key password repeatedly when launching new terminal

## Known Issues

- When installing on WSL, you may be required to change your preferred terminal's font to one of the powerline fonts (Example, `DejaVu Sans Mono for Powerline`) for the characters to show properly
- This may be required, even while installing on a dedicated ubuntu GUI installation
